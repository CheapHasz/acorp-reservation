<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 21/12/2016
 * Time: 17:32
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('room', EntityType::class, array(
                'class' => 'AppBundle\Entity\Room',
                'label' => 'Salle'
            ))
            ->add('date', DateType::class, array(
                'label' => 'Jour'
            ))
            ;
    }
}