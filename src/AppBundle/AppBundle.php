<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    /**
     * {@inheritdoc}
     *
     * La surcharge du FOSUserBundle permet de surcharger les vues de connexion/register/mot de passe oublié
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
