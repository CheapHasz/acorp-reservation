<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * AppBundle\Entity\Room
 *
 * Les différentes salles, pour lesquelles les user peuvent prendre des
 * reservations
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\RoomRepository")
 * @ORM\Table(name="room", uniqueConstraints={@ORM\UniqueConstraint(name="id_unique", columns={"id"})})
 */
class Room
{
    const DEFAULT_ID = 1;

    /**
     * id autoincrémenté
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * nom de la salle
     *
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=45)
     */
    protected $label;

    /**
     * nombre de places dans la salle
     *
     * @Assert\GreaterThanOrEqual(1)
     * @ORM\Column(type="integer")
     */
    protected $capacity;

    /**
     * On ajoute le cascade remove pour qu'à la suppression d'une salle, les reservations associées soient aussi supprimées
     *
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="room", cascade={"remove"})
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    protected $reservations;

    /**
     * Room constructor.
     */
    public function __construct()
    {
        $this->reservations = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->label;
    }
    
    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Room
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of label.
     *
     * @param string $label
     * @return \AppBundle\Entity\Room
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get the value of label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of capacity.
     *
     * @param integer $capacity
     * @return \AppBundle\Entity\Room
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get the value of capacity.
     *
     * @return integer
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Add Reservation entity to collection (one to many).
     *
     * @param \AppBundle\Entity\Reservation $reservation
     * @return \AppBundle\Entity\Room
     */
    public function addReservation(Reservation $reservation)
    {
        $this->reservations[] = $reservation;

        return $this;
    }

    /**
     * Get Reservation entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * Remove reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservations->removeElement($reservation);
    }
}
