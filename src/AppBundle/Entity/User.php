<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AppBundle\Entity\User
 *
 * extends \FOS\UserBundle\Model\User;
 *
 * Les utilisateurs, permettant de se connecter et de prendre une reservation.
 *
 * Un utilisateur particulier aura un role ROLE_ADMIN, lui permettant de se
 * connecter à l'espace d'administration.
 *
 * NB: les différents champs contenus dans le \FOS\UserBundle\Model\User ne
 * sont pas representés ici, pour éviter qu'ils soient exportés
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends \FOS\UserBundle\Model\User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="user")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $reservations;

    public function __toString()
    {
        return $this->getUsername();
    }

    public function __construct()
    {
        parent::__construct();
        $this->reservations = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \AppBundle\Entity\User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add Reservation entity to collection (one to many).
     *
     * @param \AppBundle\Entity\Reservation $reservation
     * @return \AppBundle\Entity\User
     */
    public function addReservation(Reservation $reservation)
    {
        $this->reservations[] = $reservation;

        return $this;
    }

    /**
     * Get Reservation entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    public function __sleep()
    {
        return array('Id');
    }

    /**
     * Remove reservation
     *
     * @param \AppBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\AppBundle\Entity\Reservation $reservation)
    {
        $this->reservations->removeElement($reservation);
    }
}
