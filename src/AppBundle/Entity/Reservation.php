<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * AppBundle\Entity\Reservation
 *
 * Les reservations
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ReservationRepository")
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="fk_reservation_room_idx", columns={"room_id"}), @ORM\Index(name="fk_reservation_user1_idx", columns={"user_id"})}, uniqueConstraints={@ORM\UniqueConstraint(name="id_unique", columns={"id"})})
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * la date et heure de début
     *
     * @ORM\Column(type="datetime")
     */
    protected $start;

    /**
     * la date et heure de fin
     *
     * @ORM\Column(type="datetime")
     */
    protected $end;

    /**
     * Nombre de participants
     * @Assert\GreaterThanOrEqual(1)
     * @ORM\Column(name="number_of_attendees", type="integer")
     */
    protected $numberOfAttendees;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="reservations")
     * @ORM\JoinColumn(name="room_id", referencedColumnName="id", nullable=false)
     */
    protected $room;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reservations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;

    public function __construct()
    {
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \AppBundle\Entity\Reservation
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of start.
     *
     * @param \DateTime $start
     * @return \AppBundle\Entity\Reservation
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get the value of start.
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set the value of end.
     *
     * @param \DateTime $end
     * @return \AppBundle\Entity\Reservation
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get the value of end.
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set the value of numberOfAttendees.
     *
     * @param integer $numberOfAttendees
     * @return \AppBundle\Entity\Reservation
     */
    public function setNumberOfAttendees($numberOfAttendees)
    {
        $this->numberOfAttendees = $numberOfAttendees;

        return $this;
    }

    /**
     * Get the value of numberOfAttendees.
     *
     * @return integer
     */
    public function getNumberOfAttendees()
    {
        return $this->numberOfAttendees;
    }

    /**
     * Set Room entity (many to one).
     *
     * @param \AppBundle\Entity\Room $room
     * @return \AppBundle\Entity\Reservation
     */
    public function setRoom(Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get Room entity (many to one).
     *
     * @return \AppBundle\Entity\Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set User entity (many to one).
     *
     * @param \AppBundle\Entity\User $user
     * @return \AppBundle\Entity\Reservation
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get User entity (many to one).
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __sleep()
    {
        return array('Id', 'RoomId', 'UserId', 'Start', 'End', 'NumberOfAttendees');
    }
}
