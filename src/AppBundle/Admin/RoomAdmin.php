<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 21/12/2016
 * Time: 17:06
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RoomAdmin extends AbstractAdmin
{
    /**
     * Definition des champs de listing
     *
     * @param ListMapper $list
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('label', null, array('label' => 'Libellé'))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                )
            ))
            ;
    }

    /**
     * Definition des champs du formulaire d'édition
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('label', TextType::class, array(
                'label' => 'Libellé'
            ))
            ->add('capacity', IntegerType::class, array(
                'label' => 'Capacité de la salle'
            ))
            ;
    }
}