<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 21/12/2016
 * Time: 17:41
 */

namespace AppBundle\Services;

use AppBundle\Entity\Room;
use AppBundle\Model\Filter;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class FilterSessionHandler
 *
 * Permet de stocker et de récupérer le filtre depuis la session
 *
 * @package AppBundle\Services
 */
class FilterSessionHandler
{
    /** la clé de session dans laquelle est stocké le filtre */
    const SESSION_KEY = 'filter';
    /** @var Session  */
    protected $session;
    /** @var EntityManager  */
    protected $em;

    /**
     * FilterSessionHandler constructor.
     *
     * @param Session       $session    Le composant permettant d'accèder à la session
     * @param EntityManager $em         Le manager d'entité de doctrine, permet d'intergagir avec la BDD
     */
    public function __construct(Session $session, EntityManager $em)
    {
        $this->session = $session;
        $this->em = $em;
    }

    /**
     * Récupère le filtre en session, ou le crée
     *
     * @return Filter
     */
    public function get()
    {
        $serializedFilter = $this->session->get(self::SESSION_KEY);
        if ($serializedFilter) {
            // Si quelque chose est en session, on tente de le récupérer et de le deserialiser
             $filter = unserialize($serializedFilter);
             if ($filter instanceof Filter) {
                 // si c'est bien un filtre, on le retourne, en prenant soin de faire manager les entités liées (ie les salles)
                 return $this->mergeEntities($filter);
             }
        }
        // sinon, on a pas pu récuperer de filtre en session
        // on en crée donc un
        $filter = new Filter();
        // on initialise les valeurs avec la salle par défaut
        $filter->setRoom($this->em->getRepository('AppBundle:Room')->find(Room::DEFAULT_ID));
        // et la date d'aujourd'hui
        $filter->setDate(new \DateTime());
        return $filter;
    }

    /**
     * Sauvegarde le filtre en session
     *
     * @param Filter $filter
     */
    public function set(Filter $filter)
    {
        $this->session->set(self::SESSION_KEY, serialize($filter));
    }

    /**
     * Les entités serialisées ne sont pas managées par doctrine
     * Il convient donc de les récupérer
     *
     * @param Filter $filter
     *
     * @return Filter
     */
    protected function mergeEntities(Filter $filter)
    {
        if ($filter->getRoom() instanceof Room) {
            $room = $this->em->getRepository('AppBundle:Room')->find($filter->getRoom()->getId());
            $filter->setRoom($room);
        }
        return $filter;
    }
}