<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 22/12/2016
 * Time: 09:13
 */

namespace AppBundle\Services;

use AppBundle\Entity\Reservation;
use AppBundle\Entity\Room;
use AppBundle\Model\Filter;
use AppBundle\Model\AgendaTree;
use AppBundle\Model\Day;
use AppBundle\Model\TimeSlot;
use Doctrine\ORM\EntityManager;

class AgendaTreeProvider
{
    // L'heure a laquelle commence la journée
    const START_HOUR = 8;
    // L'heure a laquelle finit la journée
    const END_HOUR = 19;


    const DATE_KEY_FORMAT = 'Y-m-d H:i:s';

    /** @var EntityManager  */
    protected $em;

    /**
     * AgendaTreeProvider constructor.
     *
     * @param EntityManager $em     Le manager d'entité de doctrine, permet d'intergagir avec la BDD
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Créé l'arbre d'agenda représentant la semaine, avec les jours et les creneaux
     *
     * @param Filter $filter
     *
     * @return AgendaTree
     */
    public function provideAgendaTree(Filter $filter)
    {
        $monday = clone $filter->getDate();
        if ($monday->format('N') != 1) {
            // si le jour choisi n'est pas un lundi
            // on choisi le lundi précedent
            $monday->modify('last monday');
        }
        // puis le vendredi suivant
        $friday = clone $monday;
        $friday->modify('next friday');

        $agendaTree = new AgendaTree();
        // on récupère toutes les reservations de la semaine
        $reservations = $this->getReservations($filter->getRoom(),$monday, $friday);
        // puis on prend un curseur, posé sur le lundi
        $cursorDate = clone $monday;
        while ($cursorDate <= $friday) {
            $agendaTree->addDay($this->provideDay($cursorDate, $reservations));
            // on ajoute une journée
            $cursorDate->modify('+1 day');
            // et on remet l'heure à minuit, pour que le test de la boucle fonctionne
            $cursorDate->setTime(0,0,0);
        }
        return $agendaTree;
    }

    /**
     * Crée et retourne le jour, avec ses creneaux, pour $date
     *
     * @param \DateTime $cursorDate         la date du jour
     * @param array     $reservations       le tableau des reservations du jour
     *
     * @return Day
     */
    protected function provideDay(\DateTime $cursorDate, array $reservations)
    {
        // on met la date à l'heure de début
        $cursorDate->setTime(self::START_HOUR,0,0);
        $day = new Day();
        $day->setDate(clone $cursorDate);
        while ($cursorDate->format('H') < self::END_HOUR) {
            $day->addTimeSlot($this->provideTimeSlot($cursorDate, $reservations));
            $cursorDate->modify(sprintf('+%d minutes', AgendaTree::SLOT_DURATION));
        }
        return $day;
    }

    /**
     * Crée le crenaux qui démarre à $cursorDate
     *
     * @param \DateTime $cursorDate
     * @param array     $reservations
     *
     * @return TimeSlot
     */
    protected function provideTimeSlot(\DateTime $cursorDate, array $reservations)
    {
        $timeSlot = new TimeSlot();
        $timeSlot->setStart(clone $cursorDate);
        if (array_key_exists($cursorDate->format(self::DATE_KEY_FORMAT), $reservations)) {
            // si une reservation existe pour cette heure la, on l'affecte
            $timeSlot->setReservation($reservations[$cursorDate->format(self::DATE_KEY_FORMAT)]);
        }
        return $timeSlot;

    }

    /**
     *  Retourne les reservations de la période, indexées par date de départ
     *
     * @param Room      $room
     * @param \DateTime $start
     * @param \DateTime $end
     *
     * @return array
     */
    protected function getReservations(Room $room, \DateTime $start, \DateTime $end)
    {
        $reservations = $this->em->getRepository('AppBundle:Reservation')->getForRoomBetweenDates($room, $start, $end);

        $mappedReservations = array();
        /** @var Reservation $reservation */
        foreach ($reservations as $reservation) {
            // on crée un tableau, en indexant les objets par la valeur start
            $mappedReservations[$reservation->getStart()->format(self::DATE_KEY_FORMAT)] = $reservation;
        }

        return $mappedReservations;
    }
}