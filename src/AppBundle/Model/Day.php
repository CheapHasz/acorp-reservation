<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 22/12/2016
 * Time: 09:35
 */

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Day
 *
 * Represente un jour de l'agenda, et est composé de créneaux
 *
 * @package AppBundle\Model
 */
class Day
{
    /** @var  \DateTime */
    protected $date;
    /** @var  ArrayCollection */
    protected $timeSlots;

    /**
     * Day constructor.
     */
    public function __construct()
    {
        $this->timeSlots = new ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return Day
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTimeSlots()
    {
        return $this->timeSlots;
    }

    /**
     * @param ArrayCollection $timeSlots
     *
     * @return Day
     */
    public function setTimeSlots($timeSlots)
    {
        $this->timeSlots = $timeSlots;

        return $this;
    }

    /**
     * @param TimeSlot $timeSlot
     *
     * @return $this
     */
    public function addTimeSlot(TimeSlot $timeSlot)
    {
        $this->timeSlots[] = $timeSlot;

        return $this;
    }
}