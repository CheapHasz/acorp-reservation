<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 22/12/2016
 * Time: 09:37
 */

namespace AppBundle\Model;

use AppBundle\Entity\Reservation;

/**
 * Class TimeSlot
 *
 * représente un creneau, avec éventuellement une reservation si elle existe
 *
 * @package AppBundle\Model
 */
class TimeSlot
{
    /** @var  \DateTime */
    protected $start;
    /** @var  Reservation|null */
    protected $reservation;

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     *
     * @return TimeSlot
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return Reservation|null
     */
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * @param Reservation|null $reservation
     *
     * @return TimeSlot
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;

        return $this;
    }
}