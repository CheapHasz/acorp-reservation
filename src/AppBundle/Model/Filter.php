<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 21/12/2016
 * Time: 17:38
 */

namespace AppBundle\Model;

use AppBundle\Entity\Room;

/**
 * Class Filter
 *
 * Utilisée pour gérer les filtres d'affichage de l'agenda
 *
 * @package AppBundle\Form\Model
 */
class Filter
{
    /** @var  Room */
    protected $room;
    /** @var  \DateTime */
    protected $date;

    /**
     * @return Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param Room $room
     *
     * @return Filter
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return Filter
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

}