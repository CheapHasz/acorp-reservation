<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 22/12/2016
 * Time: 09:30
 */

namespace AppBundle\Model;

use AppBundle\Entity\Room;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class AgendaTree
 *  Représente une semaine pour une salle de réunion.
 *  Est composé de jours
 *
 * @package AppBundle\Model
 */
class AgendaTree
{
    /**
     * la duree d'un creneau proposé
     * déclarée dans cette classe pour avoir un accès pratique dans la vue
     */
    const SLOT_DURATION = 30;
    /** @var  Room */
    protected $room;
    /** @var  ArrayCollection */
    protected $days;

    /**
     * AgendaTree constructor.
     */
    public function __construct()
    {
        $this->days = new ArrayCollection();
    }

    /**
     * @return Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param Room $room
     *
     * @return AgendaTree
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param ArrayCollection $days
     *
     * @return AgendaTree
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @param Day $day
     *
     * @return $this
     */
    public function addDay(Day $day)
    {
        $this->days[] = $day;

        return $this;
    }
}