<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reservation;
use AppBundle\Entity\Room;
use AppBundle\Model\AgendaTree;
use AppBundle\Model\Filter;
use AppBundle\Form\Type\FilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    /**
     * @Route("/", name="homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // on recupère le filtre en session
        /** @var Filter $filter */
        $filter = $this->get('app.filter_session_handler')->get();
        $filterForm = $this->createForm(FilterType::class, $filter);
        // et on récupère l'agendaTree correspondant au filtre
        $agendaTree = $this->get('app.agenda_tree_provider')->provideAgendaTree($filter);
        // et on rend la vue
        return $this->render('AppBundle:Front:index.html.twig', array(
            'filterForm' => $filterForm->createView(),
            'room' => $filter->getRoom(),
            'agendaTree' => $agendaTree
        ));
    }

    /**
     * @Route("/creer-une-reservation", name="create_reservation")
     *
     * @param Request $request      Un objet symfony permettant d'accèder à des valeur de la requête
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createReservationAction(Request $request)
    {
        // l'objet request nous interesse ici, en particulier car son attribut request permet d'accèder aux valeurs de $_POST de façon élégante
        if (!($request->request->has('number') && $request->request->has('room') && $request->request->has('start'))) {
            // le formulaire n'est pas complet
            $this->addFlash('error', 'les données sont incorrectes');
        } else {
            /** @var Room $room */
            $room = $this->getDoctrine()->getRepository('AppBundle:Room')->find($request->request->get('room'));
            if (!$room) {
                // salle non trouvée
                $this->addFlash('error', 'Salle non trouvée');
            } elseif ($request->request->get('number') > $room->getCapacity()) {
                // capacité dépassée
                $this->addFlash('error', sprintf('La salle ne peut contenir que %d personnes', $room->getCapacity()));
            } else {
                $startDate = new \DateTime($request->request->get('start'));
                $existingReservation = $this->getDoctrine()->getRepository('AppBundle:Reservation')->findOneBy(array(
                    'room' => $room,
                    'start' => $startDate
                ));
                if ($existingReservation) {
                    // une reservation existe deja pour ce moment
                    $this->addFlash('error', 'Une reservation existe deja pour ce moment');
                } elseif ($startDate < new \DateTime()) {
                    // Il est impossible de reserver dans le passé
                    $this->addFlash('error', 'Il est impossible de reserver dans le passé');
                } else {
                    try {
                        // tout est valide, on peut sauvegarder la reservation
                        $reservation = new Reservation();
                        $reservation->setStart($startDate);
                        $reservation->setRoom($room);
                        $reservation->setUser($this->getUser());
                        $reservation->setNumberOfAttendees($request->request->get('number'));
                        // la date de fin n'est mise à jour que pour permettre une évolution éventuelle
                        $endDate = clone $startDate;
                        $endDate->modify(sprintf('+%d minutes', AgendaTree::SLOT_DURATION));
                        $reservation->setEnd($endDate);
                        // puis on persist la reservation, pour que doctrine la manage
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($reservation);
                        // et on flush, ie on sauvegarde les changements
                        $em->flush();
                        $this->addFlash('success', 'Votre reservation est prise en compte');
                    } catch (\Exception $e) {
                        $this->addFlash('error', 'Une erreur s\'est produite');
                    }
                }
            }
        }
        // on redirige vers la page d'agenda
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * @Route("/supprimer-une-reservation/{reservation}", name="delete_reservation")
     *
     * @param Reservation $reservation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteReservationAction(Reservation $reservation)
    {
        if ($reservation->getUser()->getId() != $this->getUser()->getId()) {
            $this->addFlash('error', 'Cette reservation ne vous appartient pas');
        } elseif ($reservation->getStart() < new \DateTime()) {
            // Il est impossible de reserver dans le passé
            $this->addFlash('error', 'Il est impossible de supprimer une reservation dans le passé');
        } else {
            try {
                // puis on remove la reservation, pour que doctrine la supprime
                $em = $this->getDoctrine()->getManager();
                $em->remove($reservation);
                // et on flush, ie on sauvegarde les changements
                $em->flush();
                $this->addFlash('success', 'Votre reservation a été supprimée');
            } catch (\Exception $e) {
                $this->addFlash('error', 'Une erreur s\'est produite');
            }
        }
        // on redirige vers la page d'agenda
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * @Route("/changer-les-filtres", name="change_filters")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeFiltersAction(Request $request)
    {
        /** @var Filter $filter */
        $filter = $this->get('app.filter_session_handler')->get();
        $filterForm = $this->createForm(FilterType::class, $filter);
        // methode de symfony permettant d'associer les valeurs postées dans la requete à l'objet filter
        $filterForm->handleRequest($request);
        if ($filterForm->isValid()) {
            // le formulaire est considéré valide
            // on sauvegarde les valeurs
            $this->get('app.filter_session_handler')->set($filter);
        } else {
            $this->addFlash('error', 'Le formulaire est invalide');
        }
        // on redirige vers la page d'agenda
        return $this->redirect($this->generateUrl('homepage'));
    }
}
