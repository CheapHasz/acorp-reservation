<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 22/12/2016
 * Time: 18:06
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;

/**
 * Class RegistrationController
 *
 * La surcharge du controller permet de surcharger le système de redirection à la création de compte
 *
 * @package AppBundle\Controller
 */
class RegistrationController extends BaseController
{
    /**
     *  Action appelée quand la création de compte réussi
     *  Redirection directement vers la page d'agenda
     *  Le fonctionnement classique du FOSUserBundle affiche une page de confirmation, inutile ici
     */
    public function confirmedAction()
    {
        return $this->redirect($this->generateUrl('homepage'));
    }
}