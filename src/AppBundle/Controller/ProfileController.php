<?php
/**
 * Created by PhpStorm.
 * User: CheapHasz
 * Date: 14/01/2017
 * Time: 12:14
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseController;

/**
 * Class ProfileController
 *
 * La surcharge du controller permet de surcharger le système de redirection au changement de mot de passe
 *
 * @package AppBundle\Controller
 */
class ProfileController extends BaseController
{
    /**
     *  Action appelée quand le changement de mot de passe a réussi
     *  Redirection directement vers la page d'agenda
     *  Le fonctionnement classique du FOSUserBundle affiche une page de profil, inutile ici
     */
    public function showAction()
    {
        return $this->redirect($this->generateUrl('homepage'));
    }
}